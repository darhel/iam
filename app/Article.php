<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded=[];
    protected $appends =["sumDescription"];



    public function getsumDescriptionAttribute()
    {
        return substr($this->description,0,300)."...";
    }

    public function user()
    {
        return $this->belongsTo("App\User","user_id");
    }
    public function comments()
    {
        return $this->hasMany('App\Comment', 'projet_id');
    }
}
