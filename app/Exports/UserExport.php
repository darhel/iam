<?php

namespace App\Exports;

use App\User;
use Carbon\Carbon;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UserExport implements FromQuery,WithMapping,WithHeadings,WithColumnFormatting,ShouldAutoSize,WithTitle
{

    use Exportable; 

    private $search,$role;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($search,$role)
    {
        $this->search =$search;
        $this->role =$role;
    }
    
    public function title(): string
    {
        return 'utilisateurs';
    }
    public function query()
    {
        ob_end_clean(); // this
        ob_start(); // and this
        return User::query()->search($this->search)->filterRole($this->role);
    }
 
   //use Exportable; 

    public function map($user): array
    {
        return [
            $user->name,
            $user->email,
            $user->role == null ? " Inconnu" : $user->role->libelle,
            $user->created_at->format('d-m-Y')

        ];
    }

    public function headings(): array
    {
        return [
            "Nom ",
            "E-mail",
            'role',
            'enregistré le'
        ];
    }

    public function columnFormats(): array
    {
        return [

        ];
    }
}

