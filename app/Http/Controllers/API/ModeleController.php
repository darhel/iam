<?php

namespace App\Http\Controllers\API;

use App\Modele;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ModeleController extends Controller
{

            /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        return  Modele::with(['pays','modeles'])->search($q)->orderBy("created_at",'desc')->paginate($per);
           
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|unique:roles|max:255',
            'marque_id' => 'required|exists:marques,id',
        ]);

        $modele = Modele::create([
            'libelle' => $request->input('libelle'),
            'marque_id' => $request->input('marque_id'),
        ]);

        return response()->json([
            'message' => 'Modele ajoutée avec succès',
            'entity' => $modele
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function show(Modele $modele)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function edit(Modele $modele)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modele $modele)
    {

        
        $validatedData = $request->validate([
            'libelle' => 'required|max:255|unique:roles,libelle,'.$modele->id,
            'marque_id' => 'required|exists:marques,id',

        ]);
       
        $modele->libelle = $request->input('libelle');
        $modele->marque_id = $request->input('marque_id');



        $modele->save();

        return response()->json([
            'message' => 'Modele modifié avec succès',
            'entity' => $modele->fresh()],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modele  $modele
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modele $modele)
    {

        //on supprime
        $modele->delete();
        return response()->json([
            'message' => 'Modele supprimé avec succès'
            ],200);

    }
}
