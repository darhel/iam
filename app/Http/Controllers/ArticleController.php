<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page"):10;
        // if(request()->expectsJSON())
        return   Article::with(['comments','user'])->withCount('comments')->orderBy("created_at",'desc')->paginate($per);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {

            DB::beginTransaction();

            $article = Article::create(
                [
                    'title' =>$request->input('title'),
                    'description' =>$request->input("description"),
                    'content' =>$request->input("content"),
                    'slug' =>"slug",
                    'user_id' =>Auth::user()->id,
                ]
                );

            DB::commit();
            return ['success'=>true,'article'=>$article->load(['comments','user'])];

            // return response()->json(['success' => true,'projet'=> $projet->load(['entreprise','creatable','comments','tasks'])->withCount('comments')],200);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Article::with(['comments','user'])->where('id',$id)->first();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        Gate::authorize('update', $article);

        try
        {
            DB::beginTransaction();


            $article->title = $request->input('title');
            $article->description = $request->input("description");
            $article->content = $request->input("content");
            $article->resource = $request->input("resources");
            $article->user_id = Auth::user()->id;
            $article->save();

            DB::commit();
            return ['success'=>true,'projet'=>$article->load(['comments','user'])];
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        Gate::authorize('delete', $article);
        //on supprime
        $article->delete();
        return response()->json(['message' => 'Projet supprimé avec succès'],200);
    }
}
