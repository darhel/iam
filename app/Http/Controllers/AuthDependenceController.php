<?php

namespace App\Http\Controllers;

use App\Pays;
use App\SecteurActivite;
use App\Competence;
use App\Formation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;



class AuthDependenceController extends Controller
{
    public function get(Request $request,  $page)
    {
        //options pour la page vehicule
        if($page =="all")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "formations"=>Formation::all(),
                "competences"=>Competence::all(),
                "tpermis"=>[],
                "voptions"=>[],
            ];
        }
        if($page =="entreprises")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "motorisations"=>[],
                "tvehicules"=>[],
                "tpermis"=>[],
                "voptions"=>[],
            ];
        }
        //options pour la page vehicule
        if($page =="membres")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "formations"=>Formation::all(),
                "competences"=>Competence::all(),
                "tpermis"=>[],
                "voptions"=>[],
            ];
        }
        if($page =="vehicule-details")
        {
            return
            [
                "assureurs"=>[],
            ];
        }
        if($page =="utilisateurs")
        {
            return
            [
                "roles"=>[],
            ];
        }
        if($page =="historiques")
        {
            return
            [
                "utilisateurs"=>[],
            ];
        }
        if($page =="pays")
        {
            return
            [
                "pays"=>Pays::all(),
            ];
        }
        if($page =="role-show")
        {
            return
            [
                "permissions"=>[],
            ];
        }
        if($page =="locations")
        {
            return
            [
                "vehicules"=>[],
                "clients"=>[],
                "chauffeurs"=>[],
                "payements"=>[]
            ];
        }
        if($page =="depenses")
        {
            return
            [
                "vehicules"=>[],
                "tdepenses"=>[]
            ];
        }
        if($page =="flux-finances")
        {
            return
                [
                    "vehicules"=>[]
                ];
        }
        if($page =="options")
        {
            return
            [
                "toptions"=>[]
            ];
        }
    }

}
