<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;


use App\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        return  Option::with(['type_option'])->search($q)->orderBy("options.created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|unique:options|max:255',
            'type_option_id' => 'required|exists:type_options,id',
        ]);

        $option= Option::create([
            'libelle' => $request->input('libelle'),
            'type_option_id' => $request->input('type_option_id'),
        ]);

        return response()->json([
            'message' => 'Option ajoutée avec succès',
            'entity' => $option
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Option $option)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|max:255|unique:options,libelle,'.$option->id,
            'type_option_id' => 'required|exists:type_options,id',

        ]);

        $option->libelle = $request->input('libelle');
        $option->type_option_id = $request->input('type_option_id');



        $option->save();

        return response()->json([
            'message' => 'Option modifiée avec succès',
            'entity' => $option->fresh()],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy(Option $option)
    {
                //on supprime
                $option->delete();
                return response()->json([
                    'message' => 'Option Supprimé avec succès'
                    ],200);
    }
}
