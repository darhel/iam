<?php

namespace App\Http\Controllers;

use App\Marque;
use App\Http\Controllers\Controller;
use App\Location;
use App\Vehicule;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;

class SearchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
       return $request->input('q') != null ? $searchResults = (new Search())
            ->registerModel(Location::class, 'numero')
            ->registerModel(Vehicule::class, 'libelle')
            ->perform($request->query('q'))->take(8): [];


    }

}
