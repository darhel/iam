<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EntrepriseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post'))
        {
            return
            [

                //Step 0
                "logo"=>"",
                "secteur_activite_principal"=> "nullable",
                "secteur_activite_id"=>"required|exists:secteur_activites,id",
                "forme_juridique"=> "required|in:EI,SUARL/EURL,SARL,SAS,SASU,SA,SNC,SCS,GIE,Autre,SP,SCI",
                "autre_activite"=>"nullable",
                "nom_commercial"=>"required",
                // "secteur_activite_principal"=>"required",


                //step 1

                'nr'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'bp'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'pays_id'=>$this->input('step')=="1" ? 'required|exists:pays,id' : "",
                'ville'=>$this->input('step')=="1" ? 'required' : "",
                'quartier'=> $this->input('step')=="1" ? 'nullable' : "",

                //step 2

                'email'=>$this->input('step')=="2" ? 'nullable|email' : "",
                'phone1'=>$this->input('step')=="2" ? 'required|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
                'phone2'=>$this->input('step')=="2" ? 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
                'siteweb'=>$this->input('step')=="2" ? ['nullable','regex:/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i']: "",
                'facebook'=>$this->input('step')=="2" ? ['nullable','regex:/^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/i'] : "",
                'twitter'=>$this->input('step')=="2" ? 'nullable' : "",
                'linkedin'=>$this->input('step')=="2" ? 'nullable' : "",



            ];
        }
        elseif($this->isMethod('patch') || $this->isMethod('put'))
        {
            return
            [

                //Step 0
                "logo"=>"",
                "secteur_activite_principal"=> "nullable",
                "secteur_activite_id"=>"required|exists:secteur_activites,id",
                "forme_juridique"=> "required|in:EI,SUARL/EURL,SARL,SAS,SASU,SA,SNC,SCS,GIE,Autre,SP,SCI",
                "autre_activite"=>"nullable",
                // "effectif"=> "required",
                "nom_commercial"=>"required",


                //step 1

                'nr'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'bp'=>$this->input('step')=="1" ? 'nullable|numeric' : "",
                'pays_id'=>$this->input('step')=="1" ? 'required|exists:pays,id' : "",
                'ville'=>$this->input('step')=="1" ? 'required' : "",
                'quartier'=> $this->input('step')=="1" ? 'nullable' : "",

                //step 1

                'email'=>$this->input('step')=="2" ? 'nullable|email' : "",
                'phone1'=>$this->input('step')=="2" ? 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
                'phone2'=>$this->input('step')=="2" ? 'nullable|regex:/^[0-9\-\(\)\/\+\s]*$/' : "",
                'siteweb'=>$this->input('step')=="2" ? 'nullable|regex:/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i' : "",
                'facebook'=>$this->input('step')=="2" ? 'nullable|regex:/^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/i' : "",
                'twitter'=>$this->input('step')=="2" ? 'nullable' : "",
                'linkedin'=>$this->input('step')=="2" ? 'nullable' : "",

            ];
        }

    }


    public function messages()
    {
        return[
            //Step 0
            "nom_commercial.required"=>"Le nom de l'entreprise est requis",
            'forme_juridique.required' =>"La forme juridique est requise",
            'forme_juridique.in' =>"Cette forme jurique est inconnue",
            // 'effectif.required' =>"L'effectif de l'entreprise est requis",
            // 'effectif.in' =>"Cet quantité d'effectif est inconnu",
            'secteur_activite_id.required' =>"Le secteur d'activité est requis",
            'secteur_activite_id.exists' =>"Le secteur d'activité est inconnu",


            //step 1


            'pays_id.required' =>"Le pays est requis",
            'ville.required' =>"La ville est requise",
            'pays_id.exists' =>"Ce pays est inconnu",

            'email.email'=>"Le format de cet e-mail est invalide",
            'phone1.regex'=>"Le n° de téléphone a un format invalide",
            'phone1.required'=>"Le n° de téléphone est obligatoire",

            'phone1.regex'=>"Le n° de téléphone a un format invalide",
            'siteweb.regex'=>"L' URL du siteweb est invalide",
            'facebook.regex'=>"L'URL de cette page facebook est invalide",


            //step 2 images


        ];
    }
}
