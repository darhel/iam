<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => "required|string|max:255",
            'email' => "required|string|email|max:255|unique:users",
            'password' => "required|string|min:8|confirmed",
            'prenom' => "required|string|max:255",
            'civilite' => "required|string",
            'pays' => "required|string",
            'secteur' => "required|string",
            'promotion' => "required|numeric",
        ];
    }

    public function messages()
    {
        return
        [
            "nom"=>"Le nom est requis",
            'email' =>"L'adresse mail est requis",
            'password' =>"Le mot de passe est requis",
            'prenom' =>"le prenom est requis",
            'civilite' =>"la civilité est requise",
            'pays' =>"Le pays est requise",
            'secteur' =>"Le secteur est  requise",
            'promotion' =>"La promotion est invalide elle doit etre un date",
        ];
    }
}
