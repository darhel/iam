<?php
namespace App\Http\repository;

use App\User;
use Illuminate\Support\Facades\Auth;

    class messageRepository{
        private $user;

        public function __construct(User $user){
            $this->user = $user;
        }

        public function getConversation(int $userId){
            $this->user->newQuery()
                ->select('name','id')
                ->where('id','!=',$userId)
                ->get();
        }
    }
