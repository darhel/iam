<?php

return [

    [
        "table"=>"assurance",
        "defined"=>"une assurance",
        "undefined"=>"l'assurance",
        "undefined_plural"=>"les assurances",
    ],
    [
        "table"=>"assureur",
        "defined"=>"un assureur",
        "undefined"=>"l'assureur",
        "undefined_plural"=>"les assureurs",
    ],
    [
        "table"=>"carte-grise",
        "defined"=>"une carte grise",
        "undefined"=>"la carte grise",
        "undefined_plural"=>"les cartes grises",
    ],
    [
        "table"=>"client",
        "defined"=>"un client",
        "undefined"=>"le client",
        "undefined_plural"=>"les clients",
    ],
    [
        "table"=>"chauffeur",
        "defined"=>"un chauffeur",
        "undefined"=>"le chauffeur",
        "undefined_plural"=>"les chauffeurs",
    ],
    [
        "table"=>"depense",
        "defined"=>"une dépense",
        "undefined"=>"la dépense",
        "undefined_plural"=>"les dépenses",
    ],
    [
        "table"=>"flux-finance",
        "defined"=>"un flux de finance",
        "undefined"=>"le flux de finance",
        "undefined_plural"=>"les flux de finances",
    ],
    [
        "table"=>"image",
        "defined"=>"une image",
        "undefined"=>"l'image",
        "undefined_plural"=>"les images",
    ],
    [
        "table"=>"location",
        "defined"=>"une location",
        "undefined"=>"la location",
        "undefined_plural"=>"les location",
    ],
    [
        "table"=>"location",
        "defined"=>"une location",
        "undefined"=>"la location",
        "undefined_plural"=>"les location",
    ],
    [
        "table"=>"marque",
        "defined"=>"une marque",
        "undefined"=>"la marque",
        "undefined_plural"=>"les marques",
    ],
    [
        "table"=>"option",
        "defined"=>"une option",
        "undefined"=>"l'option",
        "undefined_plural"=>"les options",
    ],
    [
        "table"=>"modele",
        "defined"=>"un modèle",
        "undefined"=>"le modèle",
        "undefined_plural"=>"les modèles",
    ],
    [
        "table"=>"pays",
        "defined"=>"un pays",
        "undefined"=>"le pays",
        "undefined_plural"=>"les pays",
    ],
    [
        "table"=>"permis",
        "defined"=>"un permis",
        "undefined"=>"le permis",
        "undefined_plural"=>"les permis",
    ],
    [
        "table"=>"prolongement",
        "defined"=>"un prolongement",
        "undefined"=>"le prolongement",
        "undefined_plural"=>"les prolongements",
    ],
    [
        "table"=>"permission",
        "defined"=>"une permission",
        "undefined"=>"la permission",
        "undefined_plural"=>"les permissions",
    ],
    [
        "table"=>"role",
        "defined"=>"un rôle",
        "undefined"=>"le rôle",
        "undefined_plural"=>"les rôles",
    ],
    [
        "table"=>"type_motorisation",
        "defined"=>"un type de motorisation",
        "undefined"=>"le type de motorisation",
        "undefined_plural"=>"les types de motorisation",
    ],
    [
        "table"=>"utilisateur",
        "defined"=>"un utilisateur",
        "undefined"=>"l'utilisateur",
        "undefined_plural"=>"les utilisateurs",
    ],
];
