<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Illuminate\Support\Str;
use Faker\Generator as Faker;


$factory->define(App\Article::class, function (Faker $faker) {

    return [
        'title' =>$faker->word ,
        'description' => $faker->text,
        'content' => $faker->text,
        'user_id' =>  factory(App\User::class),
        'slug' => Str::slug($faker->word ),
    ];
});
