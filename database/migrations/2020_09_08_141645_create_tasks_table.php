<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->unsignedBigInteger('projet_id')->nullable();
            $table->unsignedBigInteger('responsable_id')->nullable();
            $table->string('name');
            $table->text('description')->nullable();
            $table->date('date_debut_prev')->defautl(date('Y-m-d'));
            $table->date('date_fin_prev')->defautl(date('Y-m-d'));
            $table->integer('avancement')->default(0);
            $table->boolean('done')->default(false);
            $table->enum('statut',['En attente',"En cours", "Achevée", "Abandonnée"])->default("En attente");
            $table->morphs('creatable');

            $table->foreign('projet_id')->references('id')->on('projets')->onDelete('set null');
            $table->foreign('responsable_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
