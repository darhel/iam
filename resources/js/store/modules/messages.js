const state  =
{
    paginator:{},
    items:[],
    message:null,
    loading:false,
    loaded:false,
    currentPage:0,
    messageAsParameter:[],
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      messageAsParameter: state => {

        return state.messageAsParameter
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    message: state => {
      return state.message
  },
};

const actions =
{
  async getMessage({commit},payload)
  {
     commit('SET_LOADING',true)
     var result = await axios.get('/api/conversations/'+payload).then( response =>
          {
            commit('SET_MESSAGES',response.data)
            commit('SET_LOADING',false)
          })
      return result;
  },
   async getMessages({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

//fr
       var result = await axios.get(payload.page=="" ? '/api/conversations/' :'/api/conversations'+payload.query).then( response =>
            {
              commit('SET_MESSAGES',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateMessage({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/conversations/'+payload.id,payload).then( response =>
            {
              commit('UPDATE_MESSAGE',response.data.entreprise)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async addMessage({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/conversations',payload).then( response =>
            {
              commit('ADD_MESSAGE',response.data.entreprise)
              commit('SET_LOADING',false)
            })

        return await result

    },
    async messageAsParameter({commit},payload)
    {
      // commit('SET_LOADING',true)
       var result = await axios.get('/conversations/messageAsParameter?query='+payload).then( response =>
            {
              var data = Array.isArray(response.data.data) ? response.data.data : new Array()
              // alert(data)
              commit('MESSAGE_AS_PARAMETER',data)
              //commit('SET_LOADING',false)
            })
        return result;
    },
    async removeMessage({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/conversations/'+payload.id).then( response =>
            {
              commit('REMOVE_MESSAGE',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_MESSAGES',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_MESSAGES(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true

        }


    },
    UPDATE_MESSAGE(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_MESSAGE(state,payload)
    {
            state.items.unshift(payload)
    },
    REMOVE_MESSAGE(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },
    MESSAGE_AS_PARAMETER(state,payload)
    {
          // alert("payload"+payload)
          state.messageAsParameter = payload ? payload : new Array()
    },

    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_MESSAGE(state,payload)
    {
        state.message = payload
    },
}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
