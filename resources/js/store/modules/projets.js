
const state  =
{
    paginator:{},
    items:[],
    projet:[],
    task:null,
    projetAsParameter:[],
    loading:false,
    loaded:false,
    currentPage:0
};

const getters  = {
    moreExists: state => {
        return state.paginator.next_page_url ? true : false
      },
    loading: state => {
        return state.loading
      },
      loaded: state => {
        return state.loaded
      },
      current_page: state => {
        return state.paginator.current_page
      },
    next_page_url: state => {
        return state.paginator.next_page_url ? true : false
    },
    _items: state => {
        return state.items
    },
    total: state => {
        return state.paginator.total
    },
    projet: state => {
      return state.projet
  },
};

const actions =
{
  async getProjet({commit},payload)
  {
     commit('SET_LOADING',true)


     var result = await axios.get('/api/projets/'+payload).then( response =>
          {
            commit('SET_PROJET',response.data)
            commit('SET_LOADING',false)
            commit('SET_LOADED',true)
          })

      return result;
  },
   async getProjets({commit},payload={page:""})
    {
       commit('SET_LOADING',true)

       var result = await axios.get(payload.page=="" ? '/api/projets/' :'/api/projets/'+payload.query).then( response =>
            {
              commit('SET_PROJETS',response.data)
              commit('SET_LOADING',false)
            })
        return result;
    },
    async updateProjet({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/projets/'+payload.id,payload).then( response =>
            {

              commit('UPDATE_PROJET',response.data.projet)
              commit('SET_LOADING',false)
            })
        return result;
    },

    async updateTask({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.patch('/api/tasks/'+payload.id,payload).then( response =>
            {
                // // console.log("payload :"+payload)
                // // console.log("response :"+response)
                // // console.log("response.data :"+response.data)
                // console.log(response.data.task)
              commit('UPDATE_TASK',response.data.task)
              commit('SET_PROJET',response.data.projet)
              commit('SET_LOADING',false)
            })
        return result;
    },

    async addProjet({commit},payload)
    {

       commit('SET_LOADING',true)

       const result = await axios.post('/api/projets',payload).then( response =>
            {
              console.log(response.data.projet)
              commit('ADD_PROJET',response.data.projet)
              commit('SET_LOADING',false)

            })

        return await result

    },

    async addTask({commit},payload)
    {
       commit('SET_LOADING',true)

       const result = await axios.post('/api/tasks',payload).then( response =>
            {
              commit('ADD_TASK',response.data.task)
              commit('SET_PROJET',response.data.projet)
              commit('SET_LOADING',false)

            })

        return await result

    },
    async addComment({commit},payload)
    {
       commit('SET_LOADING',true)

       const result = await axios.post('/api/comments',payload).then( response =>
            {
              commit('ADD_COMMENT',response.data.comment)
              commit('SET_LOADING',false)

            })
        return await result
    },
    async removeComment({commit},payload)
    {
        var result = await axios.delete('/api/comments/'+payload).then( response =>
            {
              commit('REMOVE_COMMENT',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },
//

    async removeProjet({commit},payload)
    {
       commit('SET_LOADING',true)

       var result = await axios.delete('/api/projets/'+payload.id).then( response =>
            {
              commit('REMOVE_PROJET',payload)
              commit('SET_LOADING',false)
            })
      return result;
    },

    async removeTask({commit},payload)
    {
       commit('SET_LOADING',true)
       var result = await axios.delete('/api/tasks/'+payload).then( response =>
            {
              // console.log(state.projet.)
              commit('REMOVE_TASK',payload)
              // state.projet.tasks.splice(payload,1)
              commit('SET_PROJET',response.data.projet)
              commit('SET_LOADING',false)
            })
      return result;
    },

    init({commit})
    {
            commit('SET_PROJETS',{})
            // commit('SET_PROJET',{})
            commit('SET_ITEMS',[])
            commit('SET_LOADED',false)
            commit('SET_LOADING',false)
            commit('SET_CURRENT_PAGE',0)
    }
}

const mutations =
{
    SET_PROJETS(state,payload)
    {
        state.paginator = payload
        if((Object.keys(state.paginator).length === 0 && state.paginator.constructor === Object) || (Array.isArray(payload) && payload.length <= 0)  )
        {
            state.items = []

            state.loaded = true
        }

        else
        {
           if(state.paginator.current_page==1)
           {
            state.items = []
           }
           state.paginator.data.forEach(element => {
            state.items.push(element)
          });
          state.loaded = true

        }
    },
    UPDATE_PROJET(state,payload)
    {
        const index = state.items.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.items.splice(index,1,payload)
        }
    },
    ADD_PROJET(state,payload)
    {
    //   console.log(payload)
      state.items.unshift(payload)
    },
    ADD_TASK(state,payload)
    {
      // console.log("Données :"+state)
      state.projet.tasks.unshift(payload)
    },
    ADD_COMMENT(state,payload)
    {
      state.projet.comments.push(payload)
    },
    REMOVE_PROJET(state,payload)
    {
      state.items = state.items.filter(i => i.id!== payload.id)
    },
    SET_ITEMS(state,payload)
    {
        state.items = payload
    },
    SET_CURRENT_PAGE(state,payload)
    {
        state.paginator.current_page = payload
    },
    SET_LOADING(state,payload)
    {
        state.loading =payload
    },
    SET_LOADED(state,payload)
    {
        state.loaded =payload
    },
    SET_PROJET(state,payload)
    {
        state.projet = payload
    },
    UPDATE_TASK(state,payload)
    {
    const index = state.projet.tasks.findIndex(elem =>  elem.id===payload.id)
        if(index!==-1)
        {
            state.projet.tasks.splice(index,1,payload)
        }
    },
    REMOVE_TASK(state,payload)
    {
      const index = state.projet.tasks.findIndex(elem =>  elem.id===payload)

      if(index!==-1)
        {
          state.projet.tasks.splice(index,1)
        }
    },
    REMOVE_COMMENT(state,payload)
    {
      const index = state.projet.comments.findIndex(elem =>  elem.id===payload)

      if(index!==-1)
        {
          state.projet.comments.splice(index,1)
        }
    },

}

export default
{
    namespaced : true,
    state,getters,actions,mutations
}
