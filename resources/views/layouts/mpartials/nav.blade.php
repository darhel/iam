<nav class="main-header navbar navbar-expand-md navbar-light navbar-bar-blue">
    <div class="container pl-2 pr-2">
      <a href="/" class="navbar-brand">
        <img src="{{ asset('img/iam.jpg') }}" alt="Logo" class="brand-image elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light"></span>
      </a>

      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link"><i class="nav-icon mr-1 fas fa-tachometer-alt mr-1"></i>Dashboard</router-link>
          </li>
          <li class="nav-item">
          <router-link to="/trombinoscope" class="nav-link"><i class="nav-icon mr-1 far fa-image  "></i>Trombinoscope</router-link>

          </li>
          <li class="nav-item">
            <router-link to="/entreprises" class="nav-link"><i class="nav-icon mr-1 fas fa-industry mr-1"></i>Entreprises</router-link>
          </li>
          <li class="nav-item">
            <router-link to="/projets" class="nav-link"><i class="nav-icon mr-1 fas fa-book mr-1"></i>Projets</router-link>
          </li>
          <li class="nav-item">
            <router-link to="/blogs" class="nav-link"><i class="nav-icon mr-1 fas fa-book mr-1"></i>Partage d'expérience</router-link>
          </li>
        </ul>

        <!-- SEARCH FORM -->

      {{-- </div> --}}

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
          <!-- Messages Dropdown Menu -->
       @if(Auth::user()->type=='admin')
        <li class="nav-item dropdown" >
            <a id="dropdownSubMenu1" href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><i class="nav-icon mr-1 fas fa-cog mr-1"></i> Parametres</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li class="nav-item">
                <router-link to="/pays" class="dropdown-item">
                  <i class="las la-flag nav-icon mr-1"></i>Pays

                </router-link>
              </li>
              <!-- <li class="nav-item">
                <router-link to="/historiques" class="nav-link">
                  <i class="las la-history  nav-icon mr-1"></i>
                  <p>History</p>
                </router-link>
              </li> -->
              <li class="dropdown-divider"></li>
              <li class="nav-item">
                <router-link to="/roles" class="dropdown-item">
                <i class="fab fa-keycdn nav-icon mr-1"></i>
                   Rôles
                </router-link>
              </li>
              <li class="nav-item" >
                <router-link to="/utilisateurs" class="dropdown-item">
                  <i class="fas fa-users nav-icon mr-1"></i>
                  Utilisateurs
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/token" class="dropdown-item">
                <i class="las la-share-alt nav-icon mr-1"></i>
                  API
                </router-link>
              </li>

            </ul>
        </li>
      @endif

       <!-- Notifications Dropdown Menu -->
        {{-- <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">0</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header"> 0 Notification </span>
            <div class="dropdown-divider"></div>
            <a href="javascript:void(0)" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 0 nouveau message
              <span class="float-right text-muted text-sm">3 minutes</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="javascript:void(0)" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 0 demande d'ami
              <span class="float-right text-muted text-sm">12 heures</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="javascript:void(0)" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 0 nouveau rapport
              <span class="float-right text-muted text-sm">2 jours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="javascript:void(0)" class="dropdown-item dropdown-footer">Voir toutes les notifications</a>
          </div>
        </li> --}}
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)">
            <i class="fas fa-user nav-icon mr-1"></i>
            @if(Auth::check())
                {{ Auth::user()->name }}
            @else
                <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
                Not logged in
            @endif
          </a>
          <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
            <span class="dropdown-header">
                @if(Auth::check())
                <i class="fas fa-user nav-icon ml-0"></i>

                    {{ Auth::user()->name }}
                @else
                    <!-- <img  src="{{ '/storage/avatars/'.Auth::user()->avatar }}" alt="User Image" class="user-image"> -->
                    Not logged in
                @endif
            </span>
            <div class="dropdown-divider"></div>
            {{-- <a href="javascript:void(0)" class="dropdown-item">
               <i class="nav-icon fas fa-user mr-1 "></i>Profil
            </a>
            <div class="dropdown-divider"></div>
            <a href="javascript:void(0)" class="dropdown-item">
            <i class="nav-icon fas fa-cog mr-1 "></i>Parametres
            </a> --}}
            <div class="dropdown-divider"></div>
              <a href="javascript:void(0)" @click.prevent="logout" class="dropdown-item dropdown-footer"> <i class="nav-icon fas fa-power-off text-red mr-1"></i> Déconnexion</a>
          </div>
        </li>
      </ul>
      <global-search>
    </global-search>
    </div>
    </div>
  </nav>

